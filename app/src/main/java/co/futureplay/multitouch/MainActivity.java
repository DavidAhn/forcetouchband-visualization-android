package co.futureplay.multitouch;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends Activity {
    private Timer timer;
    private int logIndex = 0;
    private VisualizationView visualizationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        visualizationView = (VisualizationView) findViewById(R.id.visualization);

        displayLog("0819 sensor log 2.csv");
//        displayCapture("capture data 1.csv");
    }

    @Override
    protected void onDestroy() {
        timer.cancel();
        super.onDestroy();
    }

    private void displayPoints() {
        TextView textView = (TextView) findViewById(R.id.points);
        VisualizationRenderer visualizationRenderer = visualizationView.getRenderer();
        List<FPPressurePoint> touchPointList = visualizationRenderer.touchPointList;

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("%d\n", logIndex));

        for (int i=0; i<touchPointList.size(); i++) {
            FPPressurePoint point = touchPointList.get(i);

            if (point == null) {
                return;
            }

            stringBuilder.append(point);
            stringBuilder.append("\n");
        }

        textView.setText(stringBuilder.toString());
    }

    public void displayLog(String filename) {
        final List<String> logList = loadLogFile(filename);

        TimerTask logTimerTask = new TimerTask() {

            @Override
            public void run() {
                String capture = logList.get(logIndex);
                VisualizationRenderer renderer = visualizationView.getRenderer();
                List<FPPressurePoint> pointList = loadPoints(capture, renderer.getGridWidth(), renderer.getGridHeight());

                renderer.touchPointList.clear();

                if (pointList != null) {
                    for (FPPressurePoint point : pointList) {
                        renderer.touchPointList.add(point);
                    }
                }

                logIndex = (logIndex + 1) % logList.size();

                visualizationView.requestRender();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        displayPoints();
                    }
                });
            }
        };
        timer = new Timer();
        timer.schedule(logTimerTask, 0, 300);
    }

    public void displayCapture(String filename) {
        String capture = loadCaptureFile(filename);
        VisualizationRenderer renderer = visualizationView.getRenderer();
        List<FPPressurePoint> pointList = loadPoints(capture, renderer.getGridWidth(), renderer.getGridHeight());

        renderer.touchPointList.clear();

        if (pointList != null) {
            for (FPPressurePoint point : pointList) {
                renderer.touchPointList.add(point);
            }
        }

        visualizationView.requestRender();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                displayPoints();
            }
        });
    }

    private List<String> loadLogFile(String filename) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(getAssets().open(filename)));
            List<String> logList = new ArrayList<String>();
            String line;

            while ((line = reader.readLine()) != null) {
                logList.add(line);
            }

            return logList;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private List<FPPressurePoint> loadPoints(String capture, int width, int height) {
        String[] values = capture.split(",");
        List<FPPressurePoint> pointList = new ArrayList<FPPressurePoint>();
        int index = 0;

        for (String value : values) {
            float pressure = Float.parseFloat(value);

            if (pressure > 0) {
                FPPressurePoint point = new FPPressurePoint(index % width, index / width, pressure);
                pointList.add(point);
            }

            index++;
        }

        return pointList;
    }

    public String loadCaptureFile(String filename) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(getAssets().open(filename)));
            String line;
            StringBuilder stringBuilder = new StringBuilder();

            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }

            return stringBuilder.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
